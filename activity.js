
// No. 1 - 4:

db.usesrs.insertOne(
{
	"name": "single",
	"accommodate": "2",
	"price": 1000,
	"description": ["A simple room with all the basic necessities"],
	"rooms_available": 10,
	"isAvailable": false
}

	)

db.usesrs.insertMany( 
	[
	{
		"name": "queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accommodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	},
	{
		"name": "double",
		"accommodates": 2,
		"price": 2000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 0,
		"isAvailable": false
	},

	]
)

// No.5 - find method:
	db.usesrs.find({"name": "double"})

// No.6 

db.usesrs.updateOne(
{
	"name": "queen"
	},

	{
		$set: {
			"rooms_available": 0,
		}
	}
)

// No. 7
db.usesrs.deleteMany(
{
	"rooms_available": 0
}
	)



